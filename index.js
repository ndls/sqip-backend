const express = require('express')
const writeImage = require('./write_image')
const sqipImage = require('./sqip_image')

const app = express()
const port = process.env.PORT || 3000

//set headers
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'https://sqip.ndls.xyz')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With,Content-Type, Accept, Authorization')
  if (req.method === 'OPTIONS') {
    res.setHeader('Access-Control-Allow-Methods', 'GET')
    return res.status(200).json({})
  }
  next()
})

app.get('/', async (req, res) => {
  let image = null
  let imageSvg = null
  try {
    image = await writeImage()
    imageSvg = await sqipImage(image.localFile)
    res.status(200).json({
      svg: imageSvg,
      url: image.imageUrl
    })
  }
  catch(error) {
    res.status(500).json({status: error.message})
  }
})


app.listen(port, () => console.log(`app listening on port: ${port}!`))