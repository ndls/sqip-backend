const fs = require('fs')
const axios = require('axios')

function writeImage() {
  return new Promise((resolve, reject) => {
    axios.get('https://source.unsplash.com/random/800x600/?nature', {responseType: 'stream'})
      .then(res => {
        const imageUrl = res.request.res.responseUrl
        const name = `${Math.random().toString(36).substring(2, 15)}.${res.headers['content-type'].split('/').pop()}`
        const file = fs.createWriteStream(`/tmp/${name}`)
        res.data.pipe(file)
        file.on('finish', () => {
          resolve({
            localFile: `/tmp/${name}`,
            imageUrl: imageUrl
          })
        })
        file.on('error', error => {
          return error
        })
      })
      .catch(error => {
        reject(error)
      })
  })
}

module.exports = writeImage