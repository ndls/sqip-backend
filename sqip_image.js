const sqip = require('sqip')

function createSvg(img) {
  return new Promise((resolve, reject) => {
    let result = null
    try {
      result = sqip({
        filename: img,
        numberOfPrimitives: 8,
        blur: 12
      })
      resolve(result.svg_base64encoded)
    }
    catch(error) {
      reject(error)
    }
  })
}

module.exports = createSvg