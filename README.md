## SQIP Backend

Mangles images into blurry shapes, see it in action [here](https://sqip.ndls.xyz/).

### Build Setup

``` bash
# install dependencies
npm install

# run at your own risk
npm start
```